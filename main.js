'use strict';
function AudioPlayer() {
  this.bgaudio = new Audio('./music/audio.mp3');
  this.matchCard = new Audio('./music/hohoho.mp3');
  this.bgaudio.volume = 0.3;
  this.matchCard.volume = 0.2;
  this.bgaudio.loop = true;
  this.startMusic = function() {
    this.bgaudio.play();
  };
  this.stopMusic = function() {
    this.bgaudio.pause();
    this.bgaudio.currentTime = 0;
  };
  this.matchedCardMusic = function() {
    this.matchCard.play();
  };
}

function Game(totalTime, cards) {
  this.cardsArray = cards;
  this.totalTime = totalTime;
  this.timer = document.getElementById('time-remaining');
  this.clicks = document.getElementById('flips');
  this.audioPlayer = new AudioPlayer();
  // Старотовые настройки игры
  this.settingGame = function() {
    this.matchedCards = [];
    this.cardTocheck = null;
    this.isLockedBoard = true;
    this.timeRemaining = this.totalTime;
    this.timer.innerText = this.timeRemaining;
    this.totalClicks = 0;
    this.clicks.innerText = this.totalClicks;
    mixCards(this.cardsArray);
    setTimeout(() => {
      this.isLockedBoard = false;
      this.startCountDouwn();
      this.audioPlayer.startMusic();
    }, 500);
  };
  //Начало отсчета таймера
  this.startCountDouwn = function() {
    this.timeRemaining--;
    this.timer.innerText = this.timeRemaining;
    if (this.timeRemaining <= 0) {
      this.gameOver('game-over-text');
      return;
    }
    this.timerId = setTimeout(() => {
      this.startCountDouwn();
    }, 1000);
  };
  // Переворачивание карточек
  this.flipCard = function(card) {
    if (!this.canFlipCard(card)) return;
    card.classList.add('flip');
    this.totalClicks++;
    this.clicks.innerText = this.totalClicks;
    if (!this.cardTocheck) {
      this.cardTocheck = card;
      return;
    }
    this.checkCards(card);
  };
  // Проверка на совпадение карт
  this.checkCards = function(card) {
    let isMatch = this.cardTocheck.dataset.item === card.dataset.item;
    isMatch ? this.cardMatched(this.cardTocheck, card) : this.unFlipCard(card);
  };
  // Если карты не совпали - возвращает карты лицевой стороной вниз
  this.unFlipCard = function(card) {
    this.isLockedBoard = true;
    setTimeout(() => {
      this.cardTocheck.classList.remove('flip');
      card.classList.remove('flip');
      this.reset();
    }, 1500);
  };
  // Если карты совпали включается соответствующая мелодия
  this.cardMatched = function(card1, card2) {
    this.audioPlayer.matchedCardMusic();
    this.matchedCards.push(card1);
    this.matchedCards.push(card2);
    this.reset();
    if (this.cardsArray.length === this.matchedCards.length) {
      this.gameOver('victory-text');
    }
  };
  // Восстанавливает первичные настройки
  this.reset = function() {
    this.cardTocheck = null;
    this.isLockedBoard = false;
  };
  // Проверяет доступна ли карта для переворачивания
  this.canFlipCard = function(card) {
    return (
      !this.isLockedBoard &&
      !this.matchedCards.includes(card) &&
      card !== this.cardTocheck
    );
  };
  // Останвливает музыку и показывает соответствующий overlay
  this.gameOver = function(overlay) {
    this.audioPlayer.stopMusic();
    clearTimeout(this.timerId);
    document.getElementById(`${overlay}`).classList.add('visible');
    setTimeout(() => {
      this.cardsArray.forEach(card => {
        card.classList.remove('flip');
      });
    }, 1000);
  };
  // перемешивает карты
  function mixCards(cards) {
    cards.forEach(card => {
      const random = Math.floor(Math.random() * (15 - 0 + 1)) + 0;
      card.style.order = random;
    });
  }
}

function startGame() {
  const cards = Array.from(document.querySelectorAll('.memory__card'));
  let game = new Game(100, cards);
  const overlays = document.querySelectorAll('.overlay-text');
  overlays.forEach(overlay => {
    overlay.addEventListener('click', () => {
      overlay.classList.remove('visible');
      game.settingGame();
    });
  });
  cards.forEach(card =>
    card.addEventListener('click', () => {
      game.flipCard(card);
    })
  );
}

startGame();
